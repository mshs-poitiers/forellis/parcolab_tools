# ParCoLab Tools
*Michael Nauge, laboratoire FoReLLIS, Université de Poitiers, 2023*


## Objectifs
Ce dépôt contient un ensemble de scripts facilitant la création des fichiers XML-TEI, le format attendu pour le dépôt de corpus dans la bibliothèque de textes alignées [ParCoLab](http://parcolab.univ-tlse2.fr/).

Ces scripts sont le fruit du partenariat noué dans le cadre du projet ANR [DIVITAL](https://anr.fr/Projet-ANR-21-CE27-0004) et du projet fédérateur FoReLLIS équipe A [corpus dynamique](https://forellis.labo.univ-poitiers.fr/corpus-dynamiques/).



## ParCoLab
[ParCoLab](http://parcolab.univ-tlse2.fr/) est un corpus de textes alignés en français, serbe, anglais, espagnol, occitan et poitevin-saintongeais, destiné à la recherche en linguistique, à l’enseignement et à l’apprentissage de langues. 

C’est un corpus parallèle de plus de 32.000.000 de mots, avec des alignements au niveau des phrases manuellement vérifiés. 
Il est librement interrogeable en ligne. 

Depuis l’été 2018, ParCoLab s’est ouvert aux langues de France grâce au projet ParCoLaF.

## Principe général des scripts
1. Générer des fichiers XML-TEI headers
1. Générer des fichiers XML-TEI bodies
1. Assembler les paires de XML-TEI header et body


## Exemple de flux de traitement
1. Disposer de textes en plusieurs langues, segmentés ou alignés

    1. avec l'aligneur [Alinea](http://turing3.u-grenoble3.fr/olivier.kraif/files/Aide/Aide.htm)
    1. manuellement avec des puces numérotés *(cf. [section : Txt liste numerotée TxtAlinea ](#Txt-liste-numerotée-TxtAlinea))*
    1. segmentés depuis du texte brut *(cf. [section : TxtBrut vers TEI body](#TxtBrut-vers-TEI-body))*
    
1. Convertir les fichiers segmentés en XML-TEI body *(cf. [section : TxtAlinea vers TEI body](#TxtAlinea-vers-TEI-body))*

1. Décrire chaque fichier du corpus dans un tableur

1. Convertir chaque ligne du tableur en XML-TEI header *(cf. [section : Génération de XML-TEI Headers](#Génération-de-XML-TEI-Headers))*

1. Assembler XML-TEI Headers et Bodies *(cf. [section : Génération de XML-TEI Headers](#Génération-de-XML-TEI-Headers))*


<img src="schema.svg" alt="schema" width="800"/>


## Génération de XML-TEI Headers
1. Créer un [tableur de metadonnées](Input/metadatas/MetasNew.csv) descriptives de son corpus
1. Placer le tableur à convertir dans le sous-dossier : transform\Input\documents\metadatas
1. Executer le [notebook metasTab2TeiHeader.ipynb](transform/metasTab2TeiHeader.ipynb)
1. Vérifier le fichier généré dans le sous-dossier : transform\TeiHeader


## Génération de XML-TEI Bodies

### TxtBrut vers TEI body
1. Placer les fichiers à convertir dans le sous-dossier : transform\Input\documents\TxtBrutFormat
1. Executer le [notebook TxtBrut2TeiBody](transform/TxtBrut2TeiBody.ipynb) utilisant le script Perl d' Aleksandra Miletic
1. Vérifier les fichiers générés dans le sous-dossier : transform\TeiBody


### TxtAlinea vers TEI body
1. Placer les fichiers à convertir dans le sous-dossier : transform\Input\documents\TxtAlineaFormat
1. Executer le [notebook TxtAlinea2TeiBody.ipynb](transform/TxtAlinea2TeiBody.ipynb)
1. Vérifier les fichiers générés dans le sous-dossier : transform\TeiBody


### Txt liste numerotée TxtAlinea 
1. Placer les fichiers à convertir dans le sous-dossier : transform\Input\documents\TxtNumberedFormat
1. Executer le [notebook TxtNumberedFormat2AlineaFormat.ipynb](transform/TxtNumberedFormat2AlineaFormat.ipynb)
1. Vérifier les fichiers générés dans le sous-dossier : transform\TxtAlineaFormat


## Fusion des XML-TEI Headers avec leurs XML-TEI Bodies
1. Executer le [notebook MergeTeiHeaderWithTeiBody.ipynb](transform/MergeTeiHeaderWithTeiBody.ipynb)
1. Vérifier les fichiers générés dans le sous-dossier : transform\TeiReady

## Execution en ligne
> Pour une exécution *ponctuelle*
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmshs-poitiers%2Fforellis%2Fparcolab_tools/HEAD)

## Execution locale
> Pour une utilisation *régulière* ou si l'application distante MyBinder rencontre un problème,
il est possible de lancer les scripts sur son ordinateur personnel.

Pour cela :
1. Téléchargez sur votre bureau et décompresser l'[archive](https://gitlab.huma-num.fr/mshs-poitiers/forellis/parcolab_tools/-/archive/main/parcolab_tools-main.zip)
1. Installez l'environnement de datascientist [Anaconda](https://www.anaconda.com/products/distribution)
1. Lancez le logiciel Jupyter (inclus dans Anaconda), ce qui lancera également votre navigateur internet
1. Depuis Jupyter, naviguez dans vos dossiers pour ouvrir l'archive téléchargée et ouvrir le fichier README.MD

NB : pour une execution locale, le script *TxtBrut2TeiBody.ipynb* nécessite d'avoir une installation de l'interpreteur **Perl** :
* [installation sur windows de perl strawberry](https://strawberryperl.com/)

## Vidéo de démo.
[![link to video](https://videotheque.univ-poitiers.fr/datas/thumbs/videos/b2n2341cv876l25mnq17.jpg)](https://videotheque.univ-poitiers.fr/video.php?id=b2n2341cv876l25mnq17&link=a2znjf53tjx1m7a5ln69dwwkskh4rj)
