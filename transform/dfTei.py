# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 16:40:12 2023

@author: mnauge01
"""

# on pourrait utiliser une xslt ou jinja ou xlmify
# mais ça fait ajouter de dépendances 
# et une complexité alors que c'est assez simple en codage en dur
# même si c'est assez moche 
# mais au moins c'est facile à maintenir :-) même pour un débutant


import pandas as pd

def df2TeiHeader(df, pathDirectoryOut):
    """
    Parameters
    ----------
    df : dataframe pandas
        un tableur structuré selon les recommendations de Dejan
    pathDirectoryOut : path
        Le dossier de destination des fichier xml header générés
        Chaque ligne du tableur produira un xml dont le nom sera celui présent
        dans la colonne "file"

    Returns
    -------
    None.

    """
    
    
    for index, row in df.iterrows():
        xmlFilename = pathDirectoryOut+row["file"] 
        
        with open(xmlFilename, "w", encoding="utf8") as xmlOut:

            xmlOut.write(f'<TEI id="{row["id"]}">\r')

            xmlOut.write('\t<teiHeader>\r')
            
            xmlOut.write('\t\t<fileDesc>\r')
            
            xmlOut.write('\t\t\t<titleStmt>\r')
            
            xmlOut.write(f'\t\t\t\t<title>{row["title"]}</title>\r')
            xmlOut.write(f'\t\t\t\t<subtitle>{row["subtitle"]}</subtitle>\r')
            
            # -- authors --
            xmlOut.write('\t\t\t\t<author>\r')
            xmlOut.write('\t\t\t\t\t<name>\r')
            xmlOut.write(f'\t\t\t\t\t\t<forename>{row["author-firstname1"]}</forename>\r')
            xmlOut.write(f'\t\t\t\t\t\t<surname>{row["author-lastname1"]}</surname>\r')
            xmlOut.write('\t\t\t\t\t</name>\r')
            xmlOut.write('\t\t\t\t</author>\r')
            
            xmlOut.write('\t\t\t\t<author>\r')
            xmlOut.write('\t\t\t\t\t<name>\r')
            xmlOut.write(f'\t\t\t\t\t\t<forename>{row["author-firstname2"]}</forename>\r')
            xmlOut.write(f'\t\t\t\t\t\t<surname>{row["author-lastname2"]}</surname>\r')
            xmlOut.write('\t\t\t\t\t</name>\r')
            xmlOut.write('\t\t\t\t</author>\r')
            
            xmlOut.write('\t\t\t\t<author>\r')
            xmlOut.write('\t\t\t\t\t<name>\r')
            xmlOut.write(f'\t\t\t\t\t\t<forename>{row["author-firstname3"]}</forename>\r')
            xmlOut.write(f'\t\t\t\t\t\t<surname>{row["author-lastname3"]}</surname>\r')
            xmlOut.write('\t\t\t\t\t</name>\r')
            xmlOut.write('\t\t\t\t</author>\r')
            # -------------
            
            # -- translators --
            xmlOut.write('\t\t\t\t<respStmt>\r')
            xmlOut.write('\t\t\t\t\t<resp>translator</resp>\r')
            xmlOut.write('\t\t\t\t\t<name>\r')
            xmlOut.write(f'\t\t\t\t\t\t<forename>{row["translator-firstname1"]}</forename>\r')
            xmlOut.write(f'\t\t\t\t\t\t<surname>{row["translator-lastname1"]}</surname>\r')
            xmlOut.write('\t\t\t\t\t</name>\r')
            xmlOut.write('\t\t\t\t</respStmt>\r')

            xmlOut.write('\t\t\t\t<respStmt>\r')
            xmlOut.write('\t\t\t\t\t<resp>translator</resp>\r')
            xmlOut.write('\t\t\t\t\t<name>\r')
            xmlOut.write(f'\t\t\t\t\t\t<forename>{row["translator-firstname2"]}</forename>\r')
            xmlOut.write(f'\t\t\t\t\t\t<surname>{row["translator-lastname2"]}</surname>\r')
            xmlOut.write('\t\t\t\t\t</name>\r')
            xmlOut.write('\t\t\t\t</respStmt>\r')
            
            xmlOut.write('\t\t\t\t<respStmt>\r')
            xmlOut.write('\t\t\t\t\t<resp>translator</resp>\r')
            xmlOut.write('\t\t\t\t\t<name>\r')
            xmlOut.write(f'\t\t\t\t\t\t<forename>{row["translator-firstname3"]}</forename>\r')
            xmlOut.write(f'\t\t\t\t\t\t<surname>{row["translator-lastname3"]}</surname>\r')
            xmlOut.write('\t\t\t\t\t</name>\r')
            xmlOut.write('\t\t\t\t</respStmt>\r')
            
            xmlOut.write('\t\t\t\t<respStmt>\r')
            xmlOut.write('\t\t\t\t\t<resp>transcriber</resp>\r')
            xmlOut.write('\t\t\t\t\t<name>\r')
            xmlOut.write(f'\t\t\t\t\t\t<forename>{row["transcriber-firstname1"]}</forename>\r')
            xmlOut.write(f'\t\t\t\t\t\t<surname>{row["transcriber-lastname1"]}</surname>\r')
            xmlOut.write('\t\t\t\t\t</name>\r')
            xmlOut.write('\t\t\t\t</respStmt>\r')

            # ------------------
            
            # -- collection --
            xmlOut.write('\t\t\t\t<collectionStmt>\r')
            xmlOut.write(f'\t\t\t\t\t<collection>{row["collection"]}</collection>\r')
            xmlOut.write('\t\t\t\t\t<respStmt>\r')
            
            
            xmlOut.write('\t\t\t\t\t</respStmt>\r')
            xmlOut.write('\t\t\t\t</collectionStmt>\r')
            # ----------------

            

            xmlOut.write('\t\t\t</titleStmt>\r')
            # ----------------

            # -- editionStmt --
            xmlOut.write('\t\t\t<editionStmt>\r')
            xmlOut.write(f'\t\t\t\t<editionStmt>{row["edition"]}</editionStmt>\r')
            xmlOut.write('\t\t\t</editionStmt>\r')
            # -----------------
            
            # -- words --            
            xmlOut.write('\t\t\t<extent>\r')
            xmlOut.write(f'\t\t\t\t<measure unit="words" quantity="{row["words"]}"/>\r')
            xmlOut.write('\t\t\t</extent>\r')
            # -----------
            
            # -- publicationStmt --
            xmlOut.write('\t\t\t<publicationStmt>\r')
            
            xmlOut.write(f'\t\t\t\t<publisher>{row["publisher"]}</publisher>\r')
            xmlOut.write(f'\t\t\t\t<pubPlace>{row["pubPlace"]}</pubPlace>\r')
            xmlOut.write(f'\t\t\t\t<date>{row["date"]}</date>\r')
            xmlOut.write('\t\t\t\t<availability>\r')
            xmlOut.write(f'\t\t\t\t\t<licence>{row["licence"]}</licence>\r')
            xmlOut.write('\t\t\t\t</availability>\r')

            xmlOut.write('\t\t\t</publicationStmt>\r')

            # ---------------------
            
            xmlOut.write('\t\t\t<notesStmt>\r')
            xmlOut.write(f'\t\t\t\t<note>{row["note"]}</note>\r')
            xmlOut.write('\t\t\t</notesStmt>\r')

            # -- sourceDesc --
            xmlOut.write('\t\t\t<sourceDesc>\r')
            xmlOut.write(f'\t\t\t\t<p>{row["sourceDesc"]}</p>\r')
            xmlOut.write('\t\t\t</sourceDesc>\r')

            # -----------------

            xmlOut.write('\t\t</fileDesc>\r')
            
            
            # -- encodingDesc --
            xmlOut.write('\t\t<encodingDesc>\r')
            xmlOut.write('\t\t\t<projectDesc>\r')
            xmlOut.write(f'\t\t\t\t<p>{row["projectDesc"]}</p>\r')
            xmlOut.write('\t\t\t</projectDesc>\r')
            xmlOut.write('\t\t</encodingDesc>\r')
            # -----------------

            # -- profileDesc --
            xmlOut.write('\t\t<profileDesc>\r')
            xmlOut.write('\t\t\t<langUsage>\r')
            xmlOut.write(f'\t\t\t\t<language ident="{row["language"]}" langOri="{row["language_ori"]}"/>\r')
            xmlOut.write('\t\t\t</langUsage>\r')
            
            xmlOut.write('\t\t\t<category type="dialecte">\r')
            xmlOut.write(f'\t\t\t\t<catDesc>{row["dialecte"]}</catDesc>\r')
            xmlOut.write('\t\t\t</category>\r')

            xmlOut.write('\t\t\t<creation>\r')
            xmlOut.write(f'\t\t\t\t<date>{row["creationDate"]}</date>\r')
            xmlOut.write('\t\t\t</creation>\r')

            xmlOut.write('\t\t\t<textDesc>\r')
            xmlOut.write(f'\t\t\t\t<derivation type="{row["derivation"]}"/>\r')
            
            xmlOut.write(f'\t\t\t\t<domain type="{row["domain1"]}"/>\r')
            xmlOut.write(f'\t\t\t\t<domain type="{row["domain2"]}"/>\r')
            xmlOut.write(f'\t\t\t\t<domain type="{row["domain3"]}"/>\r')

            xmlOut.write(f'\t\t\t\t<genre type="{row["genre1"]}"/>\r')
            xmlOut.write(f'\t\t\t\t<genre type="{row["genre2"]}"/>\r')
            xmlOut.write(f'\t\t\t\t<genre type="{row["genre3"]}"/>\r')
            
            xmlOut.write(f'\t\t\t\t<textForm type="{row["textForm"]}"/>\r')


            xmlOut.write('\t\t\t</textDesc>\r')
            # -----------------

            xmlOut.write('\t\t</profileDesc>\r')

            # -----------------
            
            xmlOut.write('\t</teiHeader>\r')
            
            
            
            
def df2TeiHeader_test():
    # préciser le chemin du fichier à lire
    cheminDuFichierCSV = "Input/metadatas/MetasNew.csv" 
    
    # demander à pandas de lire le fichier et garder ce tableau accessible dans une variable
    dfcsv = pd.read_csv(cheminDuFichierCSV, delimiter="\t", encoding="utf8")
    #lancer la conversion en tuilisant df2TeiHeader du script dfTei.py
    
    dfcsv = dfcsv.fillna('')
    df2TeiHeader(dfcsv,"TeiHeader/")
    
#df2TeiHeader_test()