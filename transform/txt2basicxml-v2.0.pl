#!/usr/bin/perl

use strict;
use warnings;
use utf8;

binmode STDOUT, ":encoding(UTF-8)";

my $file = shift @ARGV;
open (my $in, "<:encoding(UTF-8)", "$file") or die "Cannot open $file: $!\n";

my $head = 0;

print "<text>\n";
print "<body>\n";
print "<div>\n";
my $div_first=0;
my $previous_head=0;

while (<$in>) {
chomp;
next if (/^$/);
s/\s+$//;
s/^\t*//;
s//'/g;

### dealing with possible HTML entities




if (/^\s*(CHAPITRE|Chapitre)|^\s*(CHAPTER|Chapter)|^\s*(Capitol|CAPITOL)|^\s*[IVXLC]+$|^\s*[0-9]+$|^\s*[A-ZÉÈÊÀÂÙÛÔÎ0-9ĐŠŽĆČ\.\\?\!\"\'»«,;\)\(\-\s]+$|^\s*(G\s*l\s*a\s*v\s*a|GLAVA|Poglavlje|POGLAVLJE)/) {

s/&/&amp;/g;
s/</&lt;/g;
s/>/&gt;/g;
s/"/&quot;/g;
s/'/&apos;/g;
s/¢/&cent;/g;
s/£/&pound;/g;
s/¥/&yen;/g;
s/€/&euro;/g;
s/©/&copy;/g;
s/®/&reg;/g;



 if ($div_first == 0) {
   if ($previous_head == 0) {
     print "<div>\n<head><seg>$_</seg></head>\n";
     $previous_head = 1;
   } else {
     print "<head><seg>$_</seg></head>\n";
   }
 $div_first = 1;
 }
 else {

   if ($previous_head == 0) {
    print "</div>\n<div>\n<head><seg>$_</seg></head>\n";
    $previous_head=1;
   }
   else {
     print "<head><seg>$_</seg></head>\n";
   }
 }

# $previous_head=1;
}

else {
$previous_head=0;

s/&/&amp;/g;
s/</&lt;/g;
s/>/&gt;/g;
s/"/&quot;/g;
s/'/&apos;/g;
s/¢/&cent;/g;
s/£/&pound;/g;
s/¥/&yen;/g;
s/€/&euro;/g;
s/©/&copy;/g;
s/®/&reg;/g;

    my @sentences = segmente($_);

    print "<p>\n";
    foreach my $s (@sentences) {
    print "<s>$s</s>\n";
    }
    print "</p>\n";
    

}
  



}

print "</div>\n";
print "</body>\n";
print "</text>\n";


close $in;

### SEGMENTING SUB TAKEN FROM segmenteur.pl 
# SEGMENTEUR.PL --- 
# Copyright (C) 2004 

# Author: Ludovic Tanguy <Ludovic.Tanguy@univ-tlse2.fr>
# Maintainer: Ludovic Tanguy <Ludovic.Tanguy@univ-tlse2.fr>
# Created: 24 Jun 2004
# Version: 1.0
 
sub segmente
{
    my $string = shift;
    my @stringsegm ;

    # SUBST1 - fin de phrases
    $string =~ s/([^A-Z][\.\?!])([\"\*·_\-\s]+)([A-ZÉÈÊÀÂÙÛÔĐŠŽĆČ0-9])/$1<eos>$2$3/go;
    # eg : fin de phrase. Debut de phrase => fin de phrase.<eos>Debut de phrase
    # eg : fin de phrase... Debut de phrase => fin de phrase...<eos>Debut de phrase
    # eg : fin de phrase."Debut de phrase => fin de phrase.<eos>"Debut de phrase

    # SUBST 2
    $string =~ s/\s*\|[\"\*·_\-\s]*([A-ZÉÈÊÀÂÙÛÔĐŠŽĆČ])/<eos>$1/go;

    # SUBST 3
    $string =~ s/\s>+\s*([A-ZÉÈÊÀÂÙÛÔĐŠŽĆČ])/<eos>$1/go;

    # caracteres reserves
    $string =~ s/\|/ /go;
    $string =~ s/__/ /go;
    $string =~ s/\\/ /go;

    # exceptions ## ADDED by Aleksandra Miletic
    $string =~ s/(Mr\.*|Mrs\.*|Dr\.*|Prof\.*|M\.|MM\.|G\.|Sr\.)<eos>/$1/g;

    @stringsegm = split (/<eos>/,$string);

    return @stringsegm ;
} # sub segmente



#close $out;

#$ perl ../../segmenteur-simple.pl 4filles_tr_fr.csv | perl -pe 's/<s>\n/\t\t/' | less

